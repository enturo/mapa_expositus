<?php
error_reporting(E_ALL);
set_time_limit(1800);
ini_set('memory_limit','1024M');

if (isset($_POST['download'])) {
    $file   = $_POST['download'];
    $File   = new SplFileInfo($file);
    $Finfo  = new finfo;

    header('Content-Type: ' . $Finfo->file($file, FILEINFO_MIME_TYPE));
    header("Content-Transfer-Encoding: Binary"); 
    header("Content-disposition: attachment; filename=\"" . $File->getBasename() . "\""); 
    readfile($_SERVER['SERVER_NAME'] .'/'. $File->getBasename()); // do the double-download-dance (dirty but worky)
}

// Start the backup!
//zipData(__DIR__, __DIR__.'/backup.zip');
//echo 'Finished.';
// Here the magic happens :)
function zipData($source, $destination) {
    if (extension_loaded('zip') === true) {
        if (file_exists($source) === true) {
            $zip = new ZipArchive();
            if ($zip->open($destination, ZIPARCHIVE::CREATE) === true) {
                $source = realpath($source);
                if (is_dir($source) === true) {
                    $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
                    foreach ($files as $file) {
                        $file = realpath($file);
                        if (is_dir($file) === true) {
                            $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                        } else if (is_file($file) === true) {
                            $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                        }
                    }
                } else if (is_file($source) === true) {
                    $zip->addFromString(basename($source), file_get_contents($source));
                }
            }
            return $zip->close();
        }
    }
    return false;
}

  use \DirectoryIterator;

  function getFileList($item)
  {
    // array to hold return value
    $dir = [];
    $file = [];

    // add trailing slash if missing
    if(substr($item, -1) != "/") $item .= "/";

    // open directory for reading
    $d = new DirectoryIterator($item) or die("getFileList: Failed opening directory $item for reading");
    foreach($d as $fileinfo) {
      // skip hidden files
      if($fileinfo->isDot()) continue;
      
      if ($fileinfo->isDir())
      $dir[] = [
        'name' => "{$item}{$fileinfo}",
        'type' => ($fileinfo->getType() == "dir") ? "dir" : mime_content_type($fileinfo->getRealPath()),
        'size' => $fileinfo->getSize(),
        'lastmod' => $fileinfo->getMTime()
      ];
      else 
      $file[] = [
        'name' => "{$item}{$fileinfo}",
        'type' => ($fileinfo->getType() == "dir") ? "dir" : mime_content_type($fileinfo->getRealPath()),
        'size' => $fileinfo->getSize(),
        'lastmod' => $fileinfo->getMTime()
      ];
    }
    
    sort($dir);
    sort($file);
    
    return array_merge($dir,$file);
  }

  function getFilteredFileList($item, $ext)
  {
    $dir = [];
    $file = [];

    // add trailing slash if missing
    if(substr($item, -1) != "/") $item .= "/";

    // open directory for reading
    $d = new DirectoryIterator($item) or die("getFilteredFileList: Failed opening directory $item for reading");
    $iterator = new FileExtFilter($d, $ext);
    foreach($iterator as $fileinfo) {
      // skip hidden files
      if($fileinfo->isDot()) continue;
      if ($fileinfo->isDir())
      $retval[] = [
        'name' => "{$item}{$fileinfo}",
        'type' => ($fileinfo->getType() == "dir") ? "dir" : mime_content_type($fileinfo->getRealPath()),
        'size' => $fileinfo->getSize(),
        'lastmod' => $fileinfo->getMTime()
      ];
      else 
      $file[] = [
        'name' => "{$item}{$fileinfo}",
        'type' => ($fileinfo->getType() == "dir") ? "dir" : mime_content_type($fileinfo->getRealPath()),
        'size' => $fileinfo->getSize(),
        'lastmod' => $fileinfo->getMTime()
      ];
    }

    sort($dir);
    sort($file);

    return array_merge($dir,$file);
  }
  
  class FileExtFilter extends FilterIterator
  {
    private $fileext;

    public function __construct(Iterator $iterator, $fileext)
    {
      parent::__construct($iterator);
      $this->fileext = $fileext;
    }

    public function accept()
    {
      $file = $this->getInnerIterator()->current();
      return preg_match("/\.({$this->fileext})$/", $file);
    }
  }
  
  function deleteFile($item) {
    $Item = new SplFileInfo($item);
    
    if ($Item->isFile())
        return unlink($item) or die("Couldn't delete file: $item");
    else {
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($item, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );
        
        foreach ($files as $fileinfo) {
            $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
            $todo($fileinfo->getRealPath());
        }

        rmdir($item);
    }
  }
?>
<!doctype html>
<html class="fixed">
    <head>

        <!-- Basic -->
        <meta charset="UTF-8">

        <title>Admin tools</title>

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="vendor/animate/animate.css">

        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.css" />
        <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.css" />
        <link rel="stylesheet" href="vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

        <!-- Specific Page Vendor CSS -->
        <link rel="stylesheet" href="vendor/jquery-datatables/media/css/dataTables.bootstrap4.css" />

        <link rel="shortcut icon" href="favicon.png" />

        <!-- Theme CSS -->
        <link rel="stylesheet" href="css/theme.css" />

        <!-- Skin CSS -->
        <link rel="stylesheet" href="css/skins/default.css" />

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="css/custom.css">

        <!-- Head Libs -->
        <script src="vendor/modernizr/modernizr.js"></script>

    </head>
    <body>
    
        <?php
            switch(true) {
                case $_POST['action'] == 'zip':
                    zipData(__DIR__, __DIR__.'/'.$_POST['filename'].'.zip');
                    echo 'Finished.';
                break;
                case $_POST['action'] == 'upload':
                    $target_path    = __DIR__;  
                    $filename       = basename( $_FILES['fileToUpload']['name']);
                    $fullPath       = $target_path.'/'.$filename;  
                      
                    if(move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $fullPath)) {  
                        echo "File uploaded successfully! [$fullPath]";  
                    } else{  
                        echo "Sorry, file not uploaded, please try again!";  
                    } 
                break;
                case isset($_POST['unzip']):
                    $zip = new ZipArchive;
                    if ($zip->open($_POST['unzip']) === TRUE) {
                        $zip->extractTo(__DIR__);
                        $zip->close();
                        echo 'ok';
                    } else {
                        echo 'failed';
                    }
                break;
                case isset($_POST['delete']):
                    deleteFile($_POST['delete']);
                break;
            }
        ?>
        <section class="container p-4">
            <form action="" method="POST" enctype="multipart/form-data">
                <fieldset>
                    <legend>Zip</legend>
                    <div><label>Názov súboru:</label><input name="filename" type="text" value="project" /><button type="submit" value="zip" name="action">Vytvoriť</button></div>
                </fieldset>
                <fieldset>
                    <legend>Upload</legend>
                    <div><label>Súbor:</label><input type="file" name="fileToUpload"/ style="display:inline"><button type="submit" value="upload" name="action">Upload</button></div>

                    <table class="table table-bordered table-striped table-condensed mb-0 table-hover">
                        <thead>
                            <tr>
                                <th class="counter">#</th>
                                <th class="file">File</th>
                                <th class="type">Type</th>
                                <th class="type">Path</th>
                                <th class="size">Size</th>
                                <th class="date">Date</th>
                                <th class="aciton">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach (getFilteredFileList(__DIR__,'zip') as $file) {
                                    $fileName   = basename($file['name']);
                                    $Date       = new DateTime;
                                    
                                    $Date->setTimestamp($file['lastmod']);
                                    ++$i;
                                    
                                    echo '<tr>';
                                        echo "<td>{$i}</td>";
                                        echo "<td>{$fileName}</td>";
                                        echo "<td>{$file['name']}</td>";
                                        echo "<td>{$file['type']}</td>";
                                        echo "<td>{$file['size']}</td>";
                                        echo "<td>{$Date->format('d.m.Y H:i:s')}</td>";
                                        echo "<td>
                                            <button type='submit' name='delete' value='{$file['name']}'>delete</button>
                                            <button type='submit' name='unzip' value='{$file['name']}'>unzip</button>
                                            <button type='submit' name='download' value='{$file['name']}'>download</button>
                                        </td>";
                                    echo '</tr>';
                                }
                            ?>
                        </tbody>
                    </table>
                                    
                </fieldset>
                
                    <h3>Files</h3>
                    <table class="table table-bordered table-striped table-condensed mb-0 table-hover">
                        <thead>
                            <tr>
                                <th class="counter">#</th>
                                <th class="file">File</th>
                                <th class="type">Type</th>
                                <th class="type">Path</th>
                                <th class="size">Size</th>
                                <th class="date">Date</th>
                                <th class="aciton">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach (getFileList(__DIR__) as $file) {
                                    $File       = new SplFileInfo($file['name']);
                                    $fileName   = basename($file['name']);
                                    $Date       = new DateTime;
                                    $downButton = null;
                                    $delButton  = null;
                                    
                                    if($File->isFile()) 
                                        $downButton = "<button type='submit' name='download' value='{$file['name']}'>download</button>";
                                        
                                    if ($File->getBasename() != 'tools.php')
                                        $delButton = "<button type='submit' name='delete' value='{$file['name']}'>delete</button>";
                                    
                                    $Date->setTimestamp($file['lastmod']);
                                    ++$j;
                                    
                                    echo '<tr>';
                                        echo "<td>{$j}</td>";
                                        echo "<td>{$fileName}</td>";
                                        echo "<td>{$file['name']}</td>";
                                        echo "<td>{$file['type']}</td>";
                                        echo "<td>{$file['size']}</td>";
                                        echo "<td>{$Date->format('d.m.Y H:i:s')}</td>";
                                        echo "<td>
                                            $delButton
                                            $downButton
                                        </td>";
                                    echo '</tr>';
                                }
                            ?>
                        </tbody>
                    </table>
            </form>
            
        </section>
    </body>
</html>
