    <!-- <div id="admin" class="p-3 col-md-4 col-lg-3 col-sm-5 col-8"> -->
    <div id="admin" class="p-3">
        <h5 class="p-2 px-3">Administrácia<button class=" btn btn-primary float-right btn-sm p-0 px-2 m-0 mr-0 ml-3 mb-1 " style="" type="button" data-toggle="collapse" data-target="#collapseAdmin" aria-expanded="true" aria-controls="collapseAdmin">-</button></h5>
        <div id="collapseAdmin" class="collapse <?php echo $isMobile ? null : 'show' ?>">
            <hr class="separator" />
            <form>
              <div class="form-row align-items-center">
                <div class="col-sm-6">
                  <label class="sr-only" for="name">Názov</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Názov</div>
                    </div>
                    <input name="data[name]" type="text" class="form-control " id="name" placeholder="Názov">
                  </div>
                </div>
                <div class="col-3">
                  <label class="sr-only" for="scalex">os x</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">X</div>
                    </div>
                    <input name="data[x]" type="number" class="form-control " id="scalex" placeholder="Os Y">
                  </div>
                </div>
                <div class="col-3">
                  <label class="sr-only" for="scaley">os y</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Y</div>
                    </div>
                    <input name="data[y]" type="number" class="form-control " id="scaley" placeholder="Os Y">
                  </div>
                </div>
                <div class="col-6">
                  <label class="sr-only" for="param-A2">Mesto</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Mesto</div>
                    </div>
                    <select name="parameter[A2]" class="form-control" id="param-A2">
                        <?php 
                            foreach ([0 => 'Nie','Áno'] as $id => $option) {
                                echo '<option value="'.$id.'">'.$option.'</option>';
                            }
                        ?>
                    </select>
                  </div>
                </div>
                <div class="col-6">
                  <label class="sr-only" for="param-A1">Okres</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Okres</div>
                    </div>
                    <select name="parameter[A1]" class="form-control" id="param-A1">
                        <?php 
                            foreach ([1 => 'Bytča','Čadca','Dolný Kubín','Kysucké Nové Mesto','Liptovský Mikuláš','Martin','Námestovo','Ružomberok','Turčianske Teplice','Tvrdošín','Žilina'] as $id => $option) {
                                echo '<option value="'.$id.'">'.$option.'</option>';
                            }
                        ?>
                    </select>
                  </div>
                </div>
                <div class="col-10 offset-md-1">
                    <hr class="separator" />
                </div>
                <div class="col-6">
                  <label class="sr-only" for="population">Počet obyvateľov</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Počet obyvateľov</div>
                    </div>
                    <input name="data[population]" type="number" class="form-control " id="population" placeholder="Počet">
                  </div>
                </div>
                <div class="col-6">
                  <label class="sr-only" for="tax">Výška pod. daní</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Výška pod. daní</div>
                    </div>
                    <input name="parameter[tax]" type="number" class="form-control " id="param-tax" placeholder="">
                  </div>
                </div>
                <div class="col-12">
                  <label class="sr-only" for="web_link">web link</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">web link</div>
                    </div>
                    <input name="parameter[web_link]" type="text" class="form-control " id="param-web_link" placeholder="">
                  </div>
                </div>
                <div class="col-6">
                  <label class="sr-only" for="web_quality">Kvalita web sídla</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Kvalita web sídla</div>
                    </div>
                    <input name="parameter[web_quality]" type="number" class="form-control " id="param-web_quality" placeholder="">
                  </div>
                </div>
                <div class="col-6 d-none">
                  <label class="sr-only" for="law">Zákonné údaje</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Zákonné údaje</div>
                    </div>
                    <input name="parameter[law]" type="number" class="form-control " id="param-law" placeholder="">
                  </div>
                </div>
                <div class="col-10 offset-md-1">
                    <hr class="separator" />
                </div>
                <div class="col-6 ">
                    <label class="control-labelX" for="param-A3">Základné údaje</label>
                  <div class="input-group mb-2">
                    <textarea name="parameter[A3]" class="form-control " id="param-A3" rows="5" placeholder=""></textarea>
                  </div>
                </div>
                <div class="col-6">
                    <label class="control-label" for="advanced_info">Doplňujúce údaje</label>
                  <div class="input-group mb-2">
                    <textarea name="parameter[advanced_info]" class="form-control " id="param-advanced_info" rows="5" placeholder=""></textarea>
                  </div>
                </div>
                <div class="col-12 ">
                    <div class="text-danger small"><b>Upozornenie</b>: Pri manipulácii so ZOOMom nebudú sedieť súradice, vyresetujte najskôr mapu </div>
                    <hr class="separator" />
                    <button id="add" disabled="disabled" type="submit" class=" btn btn-primary mb-2" name="action" value="add">Pridať</button>
                    <button id="save" disabled="disabled" type="submit" class="float-right btn btn-primary mb-2" name="action" value="save">Uložiť</button>
                </div>
              </div>
            </form>
        </div>
    </div>
