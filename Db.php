<?php
    class Db extends PDO {
        static $_instance;

        protected
        $log = true,
        $Sth;
        
        private
            $table,
            $transactionId;
       
        // Magic method clone is empty to prevent duplication of connection
        //mohol by som to vyuzit pri serializovani?
        //private function __clone() { } -- toto neviem, ci mi neodpali aj info so statementu, co mozem pri debugovani potrebovat (pripadne sa to zapisuje niekam do logu?)
            
        /**
         * Create a Database connection
         * 
         * @return self
         */
        public function __construct() {
            
            // set default options
            $driver_options = array(
                //PDO::ATTR_PERSISTENT            => true,                    // persistent connection - cannot use with custom PDOStatement
                //PDO::ATTR_STATEMENT_CLASS       => array('DbStatement', array($this)), //associate with custom Statement
                PDO::ATTR_STATEMENT_CLASS       => array('DbStatement', array()), //associate with custom Statement
                PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,  // throw exceptions
                PDO::MYSQL_ATTR_INIT_COMMAND    => 'SET NAMES "UTF8"',      // character encoding
                PDO::MYSQL_ATTR_FOUND_ROWS      => true,                    // count rows matched for updates even if no changes made
            );
        
            parent::__construct(DB_DNS, DB_USER, DB_PASS, $driver_options);
        }
        
        public static function getInstance() {
            !static::$_instance instanceof static ? static::$_instance = new static : null;
            return static::$_instance;
        }
        
        /**
         * Prepares a statement for execution
         * 
         * @param string $statement
         * @param array driver options
         * @return MYPDOStatement object
         */
        public function prepare ($statement, $driver_options = [])
        {
            return parent::prepare(trim($statement), $driver_options);
        }

        /**
         * METHOD IS DISABLED!
         * 
         * good for deleting rows
         * we dont need to "prepare" the query 
         * will pass back num of rows deleted
         * 
         * @param string $statement
         * @return DIE(ERROR_MSG);
         */
        public function exec ($statement)
        {
            die('This function (exec) has been disabled, there is no reason to use it, use "query()" instead or "prepare()" and "execute()" the query');
        }
        
        /*
        public function setSth(DbStatement $Sth) {
            $this->Sth = $Sth;
        }
        */
        
        /**
         * Get the DbStatement
         * 
         * @return null|DbStatement
         */
        public function getSth()
        {
            return $this->Sth;
        }
        
        /**
         * Get the prepared sql
         * 
         * @return string
         */
        public function getSql()
        {
            if ($this->getSth())
                return $this->getSth()->getSql();
        }
        
        /**
         * Get the translated sql
         * 
         * @return string
         */
        public function getRawSql()
        {
            if ($this->getSth())
                return $this->getSth()->getRawSql();
        }
        
        /**
         * Get rows from Database
         * 
         * @param string $sql sql query
         * @param array $bindings associated items to bind with parameters
         * @return array of Std Objects
         */
        public function fetch ($sql, array $bindings = [])
        {
            return $this->combo($sql, $bindings)->fetch();
        }
        
        /**
         * Get a row from Database
         * 
         * @param string $sql sql query
         * @param array $bindings associated items to bind with parameters
         * @return Std Object|null
         */
        public function fetchOne ($sql, array $bindings = [])
        {
            return $this->combo($sql, $bindings)->fetchOne();
        }
        
        /**
         * Get a column from Database row
         * 
         * result is always the first key no matter the name of the field/column
         * 
         * @param string $sql sql query
         * @param array $bindings associated items to bind with parameters
         * @return string|null
         */
        public function fetchSingle ($sql, array $bindings = [])
        {
            return $this->combo($sql, $bindings)->fetchSingle();
        }
        
        public function fetchGroup ($sql, array $bindings = [], $group = null)
        {
            return $this->combo($sql, $bindings)->fetchGroup($group);
        }
        
        /**
         * Get rows from Database
         * 
         * @param string $sql sql query
         * @param array $bindings associated items to bind with parameters
         * @return array
         */
        public function fetchArray ($sql, array $bindings = [])
        {
            return $this->combo($sql, $bindings)->fetchArray();
        } 
        
        /**
         * DONT KNOW ABOUT THIS FUNCTION - doesnt seem to use it
         * 
         * @return array
         */
        //find a way to make it a single query (for loging and speed purposes)
        //TODO: disable loging for this?
        public function find ( $table, $id) {
            $this->table    = $table;
            $primaryKey     = $this->fetchSingle("SELECT `COLUMN_NAME` FROM `information_schema`.`COLUMNS`
                                                    WHERE `TABLE_SCHEMA` = '".substr(explode(';',DB_HOST)[2],7)."'
                                                      AND `TABLE_NAME` = '$table'
                                                      AND `COLUMN_KEY` = 'PRI'");
                                        
            return $this->fetchArray("SELECT * FROM $table WHERE `$primaryKey` = :id", ['id' => $id])[0];
        } 
        
        /**
         * Get rows with associated indexes
         * 
         * associates always with the first key as primary_key no matter the name of the field/column
         * 
         * @param string $sql sql query
         * @param array $bindings associated items to bind with parameters
         * @return array
         */
        public function select ($sql, array $bindings = [])
        {
            return $this->combo($sql, $bindings)->select();
        }
        
        /**
         * Insert a row into Database
         * 
         * @param string $table destination table name
         * @param array $bindings associated items to bind with parameters
         * @return DbStatement|null
         */
        public function insert ($table, array $bindings)
        {
            if ($table && !empty($bindings)) {
                $this->table        = $table;
                $sql                = sprintf("INSERT INTO `%s` (%s) VALUES (%s)", $table, static::createKeys($bindings), static::createValues($bindings));
                $Sth                = $this->combo($sql, $bindings);
                $Sth->lastInsertId  = $this->lastInsertId();

                return $Sth;
            }
        }
        
        /**
         * Update Database row(s)
         * 
         * @param string $table destination table name
         * @param array $bindings associated items to bind with parameters
         * @param string $where conditions
         * @param array $whereBindings associated items to bind with $where parameters
         * @return DbStatement|null
         */
        public function update ($table, array $bindings, $where, array $whereBindings = [])
        {
            if ($table && !empty($bindings)) {
                $data           = [];
                $this->table    = $table;
                
                foreach($bindings as $key => $val) {
                    $key    = trim($key,':');
                    $data[] = "`$key` = :$key";
                }
                
                return $this->combo(sprintf("UPDATE `%s` SET %s WHERE %s", $table, implode(', ', $data), $where), $bindings, $whereBindings);
            }
        }

        /**
         * Delete Database row(s)
         * 
         * @param string $table destination table name
         * @param string $where conditions
         * @param array $bindings associated items to bind with $where parameters
         * @return DbStatement|null
         */
        public function delete ($table, $where, array $bindings = [])
        {
            //if ($table && !empty($bindings)) {
            if ($table) {
                $this->table = $table;
                
                return $this->combo(sprintf("DELETE FROM `%s` WHERE %s", $table, $where), $bindings);
            }
        }
        
        /**
         * Get transaction id
         * 
         * @return int|null
         */
        function getTransactionId()
        {
            return $this->transactionId;
        }

        /**
         * Start a Database transaction
         * 
         * sets the transactionId with uniqueid()
         * 
         * @return bool true on success false on fail
         */
        function beginTransaction()
        {
            $this->transactionId = uniqid();
            return parent::beginTransaction();
        }
        
        /**
         * Commits all queries
         * 
         * commints all queries in transaction and reset the transactionId
         * 
         * @return bool true on success false on fail
         */
        public function commit()
        {
            $this->transactionId = null; //destroy the reference for better tracking the end of transaction
            return parent::commit();
        }

        /**
         * Rollback all queries
         * 
         * rollsback all queries in transaction and reset the transactionId
         * 
         * @return bool true on success false on fail
         */
        public function rollback()
        {
            $this->transactionId = null; //destroy the reference for better tracking the end of transaction
            return parent::rollback();
        }
       
        /**
         * Sanitize parameters
         * 
         * trims the whitespace
         * 
         * @param array $bindings associated items to bind with parameters
         * @return array
         */
        static public function sanitize (array $bindings)
        {
            foreach ($bindings as &$binding)
              if ($binding !== null) //we have to discard the 'null', it casts it to a '' string
                $binding = trim($binding);
            
            return $bindings;
        }
        
        /**
         * Create key associations
         * 
         * @param array $array keys to assoc
         * @return string
         */
        static function createKeys(array $array)
        {
            return '`'.implode('`,`',array_keys(static::sanitizeBindings($array))).'`';
        }
        
        /**
         * Create value associations
         * 
         * @param array $array values to assoc
         * @return string
         */
        static function createValues(array $array)
        {
            return ':'.implode(',:',array_keys(static::sanitizeBindings($array)));
        }
        
        /**
         * Sanitize key -> value associations
         * 
         * @param array $array keys and values to assoc
         * @return array
         */
        static protected function sanitizeBindings(array $bindings = [])
        {
            if (!empty($bindings)) {
                $keys = array_keys($bindings);
                array_walk($keys, create_function('&$str', '$str = ltrim($str,":");'));
                $bindings = array_combine($keys,$bindings);
            }
            return $bindings;
        }
        
        /**
         * Return table
         * 
         * @return string
         */
        public function getTable ()
        {
            return $this->table;
        }

        /**
         * Was there any error
         * 
         * @return bool
         */
        public function hasError ()
        {
            if ($this->getSth())
                return $this->getSth()->hasError();

            //return $this->errorCode() !== '00000';
        }
        
        static function checkDuplicity(array $sourceVars, array $data) {
            foreach ($data as $k => $v)
                if (array_key_exists($k,$sourceVars))
                    return true;
        }

        public function query ($sql, array $bindings = [], array $whereBindings = [])
        {
            //return parent::query($sql, $driver_options);
            return $this->combo($sql, $bindings, $whereBindings);
        }

        /**
         * Execute the query
         * 
         * logs every query by default
         * 
         * @param string $sql query
         * @param array $bindings associated items to bind with $where parameters
         * @param array $WhereVars associated items to bind with $where parameters
         * @return DbStatement|null
         */
        protected function combo ($sql, array $bindings = [], array $whereBindings = [])
        {
            $bindings       = static::sanitize($bindings);
            $whereBindings  = static::sanitize($whereBindings);
            $duplicity      = self::checkDuplicity($whereBindings, $bindings);
            $data           = $bindings + $whereBindings;
            
            if($duplicity)
                die ('warning, key duplicity found in sql: ' . $sql);
            
            $Sth            = self::prepare($sql)->execute($data);
            $sql            = trim(str_replace('"',null,str_replace('\'',null,$Sth->getRawSql())));
            $this->getLog() ? $this->calls[] = $Sth->createRawSql() : null;
            
            /*
            if($Sth->hasError() && strtoupper(substr(trim($sql),0,4)) != 'SHOW') { //show for loop..
                Dblog::getInstance()->logMyQuery($this); //always log if error
            } else {
                if ($this->log) {
                    if (strtoupper(substr(trim($sql),0,6)) != 'SELECT' && strtoupper(substr(trim($sql),0,4)) != 'SHOW') //dont log SELECTs and SHOWs
                        Dblog::getInstance()->setLog(false)->logMyQuery($this); //always log if error
                }
            }
            */
            
            return $Sth;
        }

        /**
         * List of table columns
         * 
         * @param string $table name
         * @return array
         */
        public function getTableColumns($table)
        {
            $this->table    = $table;
            $data           = [];

            foreach ($this->fetchArray("SHOW COLUMNS FROM `$table`") as $Field)
                $data[] = $Field['Field'];
                
            return $data;
        }

        /**
         * Get the autoIncrement value of a table
         * 
         * @param string table name
         * @return bool true on success false on fail
         */
        function getAutoIncrement($table)
        {
            $this->table = $table;
            
            return $this->fetchOne("SHOW TABLE STATUS LIKE '$table'")->Auto_increment; //next_id
        }
        
        /**
         * Logging switch
         * 
         * @param bool $switch switch
         * @return self
         */
        public function setLog ($switch = true)
        {
            $this->log = $switch;
            
            return $this;
        }
        public function getLog ()
        {
            return $this->log;
        }
    }

    class DbStatement extends PDOStatement {
        public
            //$Dbh,
            $queryString,
            $rawSql,
            $action,
            $lastInsertId,
            $rowCount,
            $executionTime,
            $date,
            $hasError,
            $errorMessage,
            $errorMessageArray,
            $transactionId,
            $parameters;
        
            
        /**
         * Executes a prepared statement
         * 
         * @param array $params Binded parameters
         * @return self DbStatement
         */
        protected function __construct(/* $Dbh */) {
            //$this->Dbh = $Dbh;
        }
        function execute($params = null)
        {
            $pageLoadStart                  = microtime(true);
            

            try
            {
                if (parent::execute($params))
                {
                    //do something
                }
                else
                {
                    throw new Exception($this->errorInfo()[2]);
                }
            }
            catch(Exception $e)
            {
                //echo $e->getMessage();
            }
                    
           
            $this->executionTime            = number_format(microtime(true) - $pageLoadStart,10); //without number_format it messes up the result (when the start and end number is identical)

            $this->parameters               = $params;
            $this->action                   = strtoupper(strtok(ltrim($this->queryString), " ")); //experimental
            $this->date                     = date('d.m.Y H:i:s');
            $this->rawSql                   = $this->createRawSql();
            $this->hasError                 = $this->errorCode() != '00000' ? 1 : 0;
            //$this->lastInsertId             = $this->Dbh->lastInsertId();
            $this->errorMessage             = $this->errorInfo()[2];
            $this->errorMessageArray        = $this->hasError() ? $this->errorInfo() : null;
            $this->rowCount                 = $this->rowCount();
            $this->transactionId            = Db::getInstance()->getTransactionId();

            //$this->Dbh->setSth($this);
            return $this;
        }
        
        /**
         * Was there any error
         * 
         * @return bool
         */
        function hasError()
        {
            return $this->hasError;
        }
        function getErrorMessage()
        {
            return $this->errorMessage;
        }

        /**
         * Get the number of rows affected by the last query
         * 
         * @return int
         */
        
        /**
         * Get the date of the last query
         * 
         * (d.m.Y H:i:s)
         * 
         * @return string (d.m.Y H:i:s)
         */
        public function getDate()
        {
            return new DateTime($this->date);
        }
        public function getCount()
        {                                 
            return $this->rowCount;
        }

        public function getSql()
        {
            return $this->queryString;
        }
        
        public function getRawSql()
        {
            return $this->rawSql;
        }

        public function getExecutionTime()
        {
            return $this->executionTime;
        }
        
        public function getParameters()
        {
            return $this->parameters;
        }
        
        public function fetch ($how = NULL, $orientation = NULL, $offset = NULL)
        {
            return $this->fetchAll(PDO::FETCH_OBJ);
        }
        
        public function fetchOne ()
        {
            if ($this->getCount())
                return current($this->fetch());
        }
        
        public function fetchSingle ()
        {
            if ($data = $this->fetchOne())
                return current($data);
        }
        
        public function fetchArray ()
        {
            return $this->fetchAll(PDO::FETCH_ASSOC);
        } 
        
        /**
         * Returns associated array with first column as the sorting key (THERE NEEDS TO BE A UNIQUE KEY)
         * 
         * @return array rows
         */
        public function fetchGroup ($groupBy = null) //for future, I leave groupBy to be set
        {
            foreach ($this->fetchAll(PDO::FETCH_ASSOC) as $array) {
                if ($groupBy == null)
                    $data[$array[key($array)]] = $array;
                else
                    $data[$array[$groupBy]] = $array;
            }
            
            return $data;
        } 
        
        public function select ()
        {
            $array = [];
            
            foreach($this->fetchArray() as $key => $subArray)
                foreach($subArray as $key2 => $val)
                    $array[$key2][$key] = $val;
            
            return $array;
        }

        /**
         * Creates a preview of executed query with real data
         * 
         * @param array $params Binded parameters
         * @return string sql
         */
        public function createRawSql()
        {
            $query = $this->queryString;
            
            foreach($this->getParameters() as $key => $value) {

                if (is_string($value))
                    $value = "'$value'";
                else 
                    //added on 09.12.2016
                    $value = is_null($value) ? 'NULL' : $value;
                    
                //replaced on 14.02.2017 (reason: WHERE `field_1` = :alias AND `field_2` = :alias) - would translate into (WHERE `field_1` = $VALUE AND `field_2` = :alias), NOTICE: I SHOULD PROBABLY MAKE SURE THE KEY IS NOT AN "?"
                //$query = preg_replace("/:$key/",print_r($value,1),$query); 
                
                //updated on 07.02.2018 - added limit, added word boundary (whole words only - consider ":idXXX = 1 and :id = 1" will result in selecting the first :id when $key = "id"; removed the print_1 - we dont accept Objects to be passed as value
                $query = preg_replace("/(:$key\b)/",$value,$query,1); 
            }
            
            return $query;
        }
    }

    /* class Dblog {
        private $Db;
        
        public function __construct() {
            $this->Db = new Db;
            $this->Db->setLog(false); //disable this option - we dont want to log the log :)
        }

        public static function getInstance() {
            !self::$_instance instanceof self ? self::$_instance = new self : null;

            return self::$_instance;
        }
        
        public function add($data) {
            $this->Db->insert('zmeny_db',$data);
            return $this->Db;
        }
        
        protected function logMyQuery ($Db, $objectId = null) {
            //TODO: I should provide this Object somehow, not expect it to be present
            global $User;

            $sth                = $Db->getSth();
            $moduleTableRowId   = 0;
            $Module             = new BrCrumbs($_SERVER["REQUEST_URI"],'modules'); //dont like this dependency :/
            
            if (!isset($User) || !$User->exists()) { //if user isnt logged in
                $User       = new stdClass;
                $User->id   = -1;
                $User->type = -1;
            }

            switch (true) {
                //custom ID specified by user
                case $objectId:
                    $moduleTableRowId   = $objectId;
                    break;
                //if lastInsertId eixists upon the last query, assume it´s an INSERT query
                case $Db->lastInsertId():
                    $moduleTableRowId = $Db->lastInsertId();
                    break;
                //try to figure out, which action has been performed
                case isset($sth->action):
                    //NOTE: this may result in NULL, when the parameter /PRIMARY_KEY/ is not provided (for example: ... WHERE `id` = 5) instead of (... WHERE `id` = :id) - //NEVIEM ZISTIT NAZOV PARAMETRU, KTORE JE ZAROVEN ID pre zmenu..
                    //NOTE: if $this->getSth()->action == 'UPDATE' - tu treba este kontrolu, ci nejaka zmena vlastne nastala (malo by vratit affected rows?) 
                    //$Db                 = new Db; //get a nec connection so I dont mess up the data
                    //$Db                 = Dblog::getInstance(); //get a nec connection so I dont mess up the data
                    $moduleTableRowId   = null;
                    
                    if ($Db->table())
                        $primaryKey     = $this->fetchOne('SHOW KEYS FROM `'.$Db->table().'` WHERE `Key_name` = "PRIMARY"')->Column_name;
                    
                    if ($primaryKey && isset($sth->parameters[$primaryKey]))
                        $moduleTableRowId = $sth->parameters[$primaryKey];
                    break;
            }
           
           $error = $errorData = null;
           $table = $Db->table();
           if($sth->hasError()) {
               $table       = $table ? $table : '';
               $error       = $sth->getErrorMessage();
               $errorData   = utf8_encode( serialize([
                        '$_GET'     => $_GET,
                        '$_POST'    => $_POST,
                        '$_SESSION' => $_SESSION,
                        '$_SERVER'  => $_SERVER,
                        '$USER'     => $User,
                    ]));
           }
           
           $bindings = array(
                'query'     => $sth->getRawSql(),
                'modul'     => $Module->lastModule(), //dependency :/
                'id'        => $moduleTableRowId,
                'kto'       => $User->id,
                'typ_konto' => $User->type,
                'url'       => $_SERVER['REQUEST_URI'],
                'akcia'     => $sth->action,
                'data'      => utf8_encode(serialize($_POST)),
                'stav'      => (int)!$sth->hasError,
                'tableName' => $table,
                'error'     => $error,
                'errorData' => $errorData,
                    
           );
            
           $this->combo(sprintf("INSERT INTO `%s` (%s) VALUES (%s)", 'zmeny_db', static::createKeys($bindings), static::createValues($bindings)),$bindings);
            
           return $this;
        } 
    }   */
    