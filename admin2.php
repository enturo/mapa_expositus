<?php error_reporting(1) ?>
<!doctype html>
    <html class="fixed" lang="sk-SK">
    <head>
        <!-- Basic -->
        <meta charset="UTF-8">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="custom.css">

        <?php
            $isMobile   = false;
            $useragent  = $_SERVER['HTTP_USER_AGENT'];

            if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
                $isMobile = true;
        ?>

        <title>Mapa Žilinský kraj</title>
        <style type="text/css">
            body {font-size:11px}
            table th {font-weight:normal; white-space: nowrap;}
        </style>
    </head>
  
    <body style="overflow:auto">
        <?php
            if ($_POST['action'] == 'upload') {
                // Excel reader from http://code.google.com/p/php-excel-reader/
                include_once ('libs/xls_reader.php');
                include_once('connect.php');
        
                if($_FILES['xls']['error'] == 0){
                    /*
                    echo '<pre>'; print_r($_FILES); echo '</pre>';


                    $filename = __DIR__.'/tmp/test.txt';
                    $somecontent = "Add this to the file\n";

                    // Let's make sure the file exists and is writable first.
                    if (is_writable($filename)) {

                        // In our example we're opening $filename in append mode.
                        // The file pointer is at the bottom of the file hence
                        // that's where $somecontent will go when we fwrite() it.
                        if (!$handle = fopen($filename, 'w')) {
                             echo "Cannot open file ($filename)";
                             exit;
                        }

                        // Write $somecontent to our opened file.
                        if (fwrite($handle, $somecontent) === FALSE) {
                            echo "Cannot write to file ($filename)";
                            exit;
                        }

                        echo "Success, wrote ($somecontent) to file ($filename)";

                        fclose($handle);

                    } else {
                        echo "The file $filename is not writable";
                    }

                    */
                    
                    $target_path = __DIR__."/tmp/";
                    $target_path = $target_path . 'src.xlsx'; 

                    if(move_uploaded_file($_FILES['xls']['tmp_name'], $target_path)) {
                        echo "Súbor ".  basename( $_FILES['xls']['name']). 
                        " bol nahraný";
                    } else{
                        echo "Chyba - súbor sa nepodarilo zapísať na server!";
                    }
                    
                    /*
                    die;
                    var_dump(file_put_contents(__DIR__.'/tmp/test.txt','textToSave'));
                    die('done');
                    
                    echo '<pre>'; print_r($_FILES); echo '</pre>';
                    //var_dump(file_put_contents(__DIR__.'/tmp/src.xls',file_get_contents($_FILES['xls']['tmp_name'])));
                    var_dump(move_uploaded_file($_FILES['xls']['tmp_name'],__DIR__.'/src.xls'));
                    die;
                    */
                }

                $Filepath       = __DIR__.'/tmp/src.xlsx'; 
                $Spreadsheet    = new SpreadsheetReader_XLSX($Filepath);
                $DbParams       = Db::getInstance()->fetchGroup('SELECT * FROM `parameter`',[],'name');
                
       
                $Spreadsheet->ChangeSheet(1);
                
                $Spreadsheet->rewind();
                
                //foreach ($Spreadsheet as $column => $city)
                foreach ($Spreadsheet->current() as $column => $city)
                    if (!in_array($column,[0,1,2])) //first three columns are descriptions
                        $xlsCities[$column] = mb_strtolower($city);
                
                foreach (Db::getInstance()->fetchArray('SELECT `id`,LOWER(`name`) as `name` FROM `item`') as $city)
                    $DbCities[$city['id']] = $city['name'];
                
                foreach ($Spreadsheet as $key => $row) {
                    if ($key == 0 || $row[1] == '') {  //skip first row (headings) or skip rows with no ID
                        continue;
                    }

                    $xls[$key]  = $row;
                    $name       = $row[1];
                    $desc       = $row[2];
                    
                    //if ($name) {
                        $params[$name]  = $desc;
                        $xls2[$name]    = $row;
                    //}
                }

                $start = 3;
                foreach ($xlsCities as $i => $city) {
                    $j = 1;
                    foreach ($params as $key => $value) {
                        $data[$city][$key] = $xls2[$key][$i];
                        $j++;
                    }
                    $start++;
                }
                
                $assocDbCities = array_flip($DbCities);
                
                //manage parameters
                Db::getInstance()->beginTransaction();
                foreach ($params as $name => $desc) {
                    if(array_key_exists($name,$DbParams)) {
                        Db::getInstance()->update('parameter',['description' => $desc],'`id` = :id',['id' => $name]);
                    } else {
                        Db::getInstance()->insert('parameter',['name' => $name,'description' => $desc]);
                    }
                }
                //echo '<pre>'; print_r(Db::getInstance()); echo '</pre>';
                //Db::getInstance()->rollback();
                
                //manage item parameters
                $DbParams       = Db::getInstance()->fetchGroup('SELECT * FROM `parameter`',[],'name');
                //Db::getInstance()->beginTransaction();
                foreach ($data as $name => $parameters) {
                    if (in_array($name,$DbCities)) {
                        $c++;
                        $rowDbParams = Db::getInstance()->fetchGroup('SELECT *,`p`.`id` as `paramId`, `i`.`id` as `itemParamId` FROM `item_parameter` `i` JOIN `parameter` `p` ON `p`.`id` = `i`.`parameter` WHERE `item` = :itemId',['itemId' => $assocDbCities[$name]],'name');
                        //echo '<pre>ROWPARAMS: '; print_r($rowDbParams); echo '</pre>';
                        foreach ($parameters as $paramName => $value) {
                            if (array_key_exists($paramName,$rowDbParams)) {
                                if ($value == '') {
                                    //$value              = null;
                                    $resetParamIds[]    = $rowDbParams[$paramName]['itemParamId'];
                                    continue;
                                }

                                Db::getInstance()->update('item_parameter',['value' => $value],'`id` = :id',['id' => $rowDbParams[$paramName]['itemParamId']]);
                            }
                            else {
                                if ($value == '')
                                    continue;

                                Db::getInstance()->insert('item_parameter',['value' => $value,'parameter' => $DbParams[$paramName]['id'], 'item' => $assocDbCities[$name]]);
                            } 
                            
                            $error[] = Db::getInstance()->hasError();
                        }
                        //Db::getInstance()->update('parameter',['description' => $desc],'`id` = :id',['id' => $name]);
                    } else {
                        //Db::getInstance()->insert('parameter',['name' => $name,'description' => $desc]);
                    }
                }
                if (!empty($resetParamIds))
                    Db::getInstance()->query('UPDATE item_parameter SET value = null WHERE id IN ('.implode(',',$resetParamIds).')');
                
                Db::getInstance()->commit();


                echo '<table class="table table-bordered table-striped table-condensed mb-0 table-hover">';
                echo "<th>Mesto/obec</th>";
                
                foreach ($params as $key => $param) {
                    echo "<th title='$param'>".$key."</th>";
                }
                foreach ($data as $name => $parameters) {
                    $class = 'class="alert alert-success"';
                    if (!in_array($name,$DbCities))
                        $class= 'class="alert alert-danger" title="Položka sa v DB nenašla"';
                    
                        
                    echo "<tr>";
                        echo "<th $class style='text-transform: capitalize'>$name</th>";
                        foreach ($parameters as $paramName => $value) {
                            $pDesc = $params[$paramName];
                            echo "<td class='text-center' title='$name: $pDesc [$paramName]'>$value</td>";
                        }
                    echo '</tr>';
                }
                echo '</table>';
                    
                //echo '<pre>assoc '; print_r($assocDbCities); echo '</pre>';
                echo '<div class="table-responsive" style="height: 700px; border:1px solid silver"><pre>RESULTS: '; print_r(Db::getInstance()); echo '</pre></div>';
                //echo '<pre>Errors: ' . $c; var_dump($error); echo '</pre>';
                echo "<br>count xls cities: " . count($xlsCities);
                echo "<br>count db cities: " . count($DbCities);
                //echo '<pre>XLS: '; print_r($xls); echo '</pre>';        
                //echo '<pre>DbParams: '; print_r($DbParams); echo '</pre>';        
                //echo '<pre>'; print_r($data); echo '</pre>';        
                //echo '<pre>XmlCities: '; print_r($xlsCities); echo '</pre>';
                //echo '<pre>Params'; print_r($params); echo '</pre>';
                //echo '<pre>Cities rows ' . count($data); print_r($data); echo '</pre>';
                //echo '<pre>XLS rows'; print_r($cities); echo '</pre>';
                /*
                echo '<pre>XLS rows'; print_r($xls); echo '</pre>';
                
                
                echo '<pre>Found: ' . count($updateCity); print_r($updateCity); echo '</pre>';
                echo '<pre>Missing: ' . count($missingCity); print_r($missingCity); echo '</pre>';
                echo '<pre>Param: ' . count($param); print_r($param); echo '</pre>';
                echo '<pre>DbParam: ' . count($DbParams); print_r($DbParams); echo '</pre>';
                */
            } else {
                echo 
                    '<form style="margin:auto;margin-top:10%;border:1px solid silver;padding:20px;width:500px;display:block" action="" method="post" enctype="multipart/form-data">
                        <label style="color:gray;margin-right:10px">Nahrať súbor XLSX na server</label>
                        <input type="file" name="xls" id="xlsUpload" /> 
                        <input type="submit" value="Nahrať" name="submit" />
                        <input type="hidden" name="action" value="upload" />
                    </form>
                    <hr />
                    ';
            }
            
        ?>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="touch.js"></script>
        <script type="text/javascript">
            $(function() {
            })
        </script>
  
    </body>
</html>