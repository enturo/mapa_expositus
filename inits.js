//var width       = document.getElementById('itemList').offsetWidth;
//var height      = document.getElementById('itemList').offsetHeight;
var accentMap = {
  "ľ": "l",
  "š": "s",
  "č": "c",
  "ť": "t",
  "ž": "z",
  "ý": "y",
  "á": "a",
  "í": "i",
  "é": "e",
  "ú": "u",
  "ä": "a",
  "ň": "n",
  "ô": "o",
  "ó": "o",
  "ď": "d",
  "ĺ": "l",
  "Ľ": "L",
  "Š": "S",
  "Č": "C",
  "Ť": "T",
  "Ž": "Z",
  "Ý": "Y",
  "Á": "A",
  "Í": "I",
  "É": "E",
  "Ú": "U",
  "Ň": "N",
  "Ó": "O",
  "Ď": "D",
  "Ĺ": "L"     
};

var normalize = function( term ) {
  var ret = "";
  for ( var i = 0; i < term.length; i++ ) {
    ret += accentMap[ term.charAt(i) ] || term.charAt(i);
  }
  return ret;
};            

var width       = 900;
var height      = 694;

var pointWidth    = 11;
var pointHeight   = 11;

var defaultZoom = 1;
var actualZoom  = defaultZoom;
var zoom        = 0.2;

var pointTop      = new Object();
var pointLeft     = new Object();

var isMobile    = false; //initiate as false

// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)))
    isMobile = true;
    
function qTipSetup(elem) {
    elem.qtip({
        position: { my: "bottom center ", at: "top center" },
        style: { classes: ' qtip qtip-green qtip-shadow qtip-rounded ' },
    });
}
function openSidePanel() {
    $('#sidePannelToggler').text('«').attr('title','zbaľ');
    $("#section").animate({ width: "show" });
}

function closeSidePanel() {
    $('#sidePannelToggler').text('»').attr('title','rozbaľ');
    $("#section").animate({ width: "hide" });
}

$(function() {
    $('#itemList').hide()
    var pointDefault  = $('#pointDefault');

    $(document).ajaxComplete(function(e,xhr,settings) {
        qTipSetup($("[title]:not('#zoomButtons button,#sidePannelToggler')"))
    })

    qTipSetup($("[title]:not('#zoomButtons button,#sidePannelToggler')"))

    $("#zoomButtons button").qtip({
        position: { my: "right center ", at: "left center" },
        style: { classes: ' qtip qtip-green qtip-shadow qtip-rounded ' },
    });

    $("#sidePannelToggler").qtip({
        position: { my: "left center ", at: "right center" },
        style: { classes: ' qtip qtip-green qtip-shadow qtip-rounded ' },
    });

    selectPoint(pointDefault);
    
    $('#sideMenu').resizable({ minWidth: 310, minHeight: 240 })
    $('#legend').resizable({ minWidth: 310, minHeight: 108 })
    $('#admin').resizable({ minWidth: 310,minHeight: 350 })
    $('#itemData').resizable({ minWidth: 310,minHeight: 200 })
    
    /*
    $('.point').each(function(i,item){
        j            = $(item).attr('rel');
        pointTop[j]    = parseInt($(item).css('top'))
        pointLeft[j]   = parseInt($(item).css('left'))
    })
    */
                    
    if (isMobile) {
        actualZoom = actualZoom - zoom - zoom - zoom;
        $('#sidePannelToggler').text('»').attr('title','rozbaľ');
        $("#section").hide();
        
        $('#sidePannelToggler, #zoomButtons button').on('click',function(){
            var self = $(this);
            
            self.qtip('api').show();
            
            setTimeout(function(){
                self.qtip('api').hide();
            }, 50);
        })
    }
    
    function zoomRefresh(width,height,actualZoom) {
        $('#itemList').width(width * actualZoom);
        $('#itemList').height(height * actualZoom);
        $('#itemList').css('background-size',width * actualZoom + 'px ' + height * actualZoom + 'px');
        
        refreshPointPositions(actualZoom);
    }
    
    zoomRefresh(width,height,actualZoom)

    $('#itemList').fadeIn('slow',function(){
        $(this).css('display','inline-table')
    });
    
    $("#itemList").draggable();
    
    $('#sidePannelToggler').on('click',function(e){
        $("#section").is(':visible') ? closeSidePanel() :openSidePanel()
        e.preventDefault()
    })
    
    $('#reset').on('click',function(){
        actualZoom = isMobile ? defaultZoom - zoom : defaultZoom;

        $('#itemList').css('top',0);
        $('#itemList').css('left',0);

        zoomRefresh(width,height,actualZoom)
    })
    
    $('#zoomIn').on('click',function(){
        actualZoom = actualZoom + zoom;

        zoomRefresh(width,height,actualZoom)
    })
    $('#zoomOut').on('click',function(){
        actualZoom = actualZoom - zoom;

        zoomRefresh(width,height,actualZoom)
    })
    
    $('#itemList').on('mousewheel wheel', function(e){
        if (typeof e.originalEvent.wheelDelta != 'undefined') {
            //chrome
            stopWheel();
            if(e.originalEvent.wheelDelta /120 > 0) {
                $('#zoomIn').click();
            //firefox
            } else {
                $('#zoomOut').click();
            }
        } else {
            // wheeled up
            if(e.originalEvent.deltaY < 0){
                $('#zoomIn').click();
            // wheeled down
            } else {
                $('#zoomOut').click();
            }
            
            e.preventDefault()
        }
    });
    

    //This should have each valid amount that can be selected in the slider 
    var lastIncrement   = 0;
    var sliderAmountMap = [];
    for (i = 0; i <= 145; i++) {
        sliderAmountMap.push(lastIncrement);
        if (i < 50) {
            lastIncrement = lastIncrement + 100;
        } else {
            lastIncrement = lastIncrement + 1000;
        }
    }

    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: sliderAmountMap.length-1, //the max length, slider will snap until this point in equal width increments
        values: [ 0, 100000 ],
      
      slide: function( event, ui ) {
        $( "#populationMin" ).text(sliderAmountMap[ui.values [0]]);
        $( "#populationMax" ).text(sliderAmountMap[ui.values [1]]);
      },
      stop: function( event, ui ) {
        $('#search').keyup()
      }
    });
    $( "#slider-range-transparency" ).slider({
        range: true,
        min: 0,
        max: 100, //the max length, slider will snap until this point in equal width increments
        values: [ 0, 100 ],
      slide: function( event, ui ) {
        $( "#transparencyMin" ).text(ui.values [0]);
        $( "#transparencyMax" ).text(ui.values [1]);
      },
      stop: function( event, ui ) {
        $('#search').keyup()
      }
    });
    $( "#slider-range,#slider-range-transparency" ).on( "change", function( event, ui ) {
        $('#search').keyup()
    } );
    
    $(document).on('keyup change','form :input',function(){
        selected.css('top',$('#scalex').val() * actualZoom);
        selected.css('left',$('#scaley').val() * actualZoom);
        
        console.log($('#scalex').val());
        console.log($('#scaley').val());
        console.log('////////////');
        
        $('#add,#save').prop('disabled',true);

        if ($('#scalex').val() > 0 && $('#scaley').val() > 0) {
            if(selected.attr('rel') != '0') {
                $('#add').prop('disabled',true);
                $('#save').prop('disabled',false);
            } else {
                $('#add').prop('disabled',false);
                $('#save').prop('disabled',true);
            }
        }
    })
    $(document).on('click','.point:not("#pointDefault")',function(){
        var self = $(this);
        selected = selectPoint(self);
        
        $('#itemData .clearClick').text('');
        
        if ($('#admin').length) {
            $('#admin :input').val('');

            $('#add,#save').prop('disabled',true);

            $.ajax({
                type    : 'post',
                url     : 'ajax.php',
                data    : "action=item&id="+self.attr('rel'),
                async   : true,
                success : function(data) {
                    Data = jQuery.parseJSON(data);                
                    $('#scalex').val(Data.x);
                    $('#scaley').val(Data.y);
                    $('#name').val(Data.name);
                    $('#population').val(Data.population);
                    $('#attribute').val(Data.attribute);

                }
            });
            
            $('#admin').find(':checkbox').prop('checked',false)
        }
         
        /*
        $.ajax({
            type    : 'post',
            url     : 'ajax.php',
            data    : "action=data&id="+self.attr('rel'),
            async   : true,
            success : function(data) {
                $('#itemData').html(data);
                
                if (isMobile)
                    openSidePanel()
            }
        });
        */
        $.ajax({
            type    : 'post',
            url     : 'ajax.php',
            data    : "action=item&id="+self.attr('rel'),
            async   : true,
            success : function(data) {
                $('#itemData').show();
                Data = jQuery.parseJSON(data);
                //console.log(data)
                
                $.each(Data, function(i,item){
                    $('#load-'+i).text(item)
                })                

                
                if (isMobile)
                    openSidePanel()
            }
        });
        
        $.ajax({
            type    : 'post',
            url     : 'ajax.php',
            data    : "action=itemParams&id="+self.attr('rel'),
            async   : true,
            success : function(params) {
                Params = jQuery.parseJSON(params);
                
                srcData     = $('#itemData');
                srcAdmin    = $('#admin');
                
                $.each(Params, function(i,Param){
                    $('#load-'+Param.name,srcData).text(Param.value)
                    
                    if ($('#param-'+Param.name,srcAdmin).is(':checkbox')) {
                        var isCity = parseInt(Param.value) ? 1 : 0;
                        $('#param-'+Param.name,srcAdmin).prop('checked',isCity)
                    } else {
                        $('#param-'+Param.name,srcAdmin).val(Param.value)
                    }
                    
                })                
            }
        });
       
    })
    
    //disabled, doing something unexpected             
    $('#dataContainer:not(.point)').on('click',function(e){
        if ($(e.target).hasClass('point') == false) {

            selected = selectPoint(pointDefault);
            
            selected.css('top',-10000);
            selected.css('left',-10000);
            
            $('form input').val('');
            $('#add,#save').prop('disabled',true);
        }
    })

    var form = $('form:eq(0)');
    $('#add').on('click',function(){
        $.ajax({
            type    : 'post',
            url     : 'ajax.php',
            data    : $(form).serialize() + "&action=add",
            async   : true,
            success : function(data) {
                location.reload();
                //Data = jQuery.parseJSON(data);                
            }
        });
        return false;
    })
    $('#save').on('click',function(){
        checkboxes = '';
        $(form).find(':checkbox').each(function(i,item){
            checked = + $(item).is(':checked'); //plus sign converts it to 0 | 1
            checkboxes += '&'+$(item).prop('name')+'='+checked;
        })
        
        $.ajax({
            type    : 'post',
            url     : 'ajax.php',
            data    : $(form).serialize() + checkboxes + "&id="+selected.attr('rel')+"&action=save",
            async   : true,
            success : function(data) {
                //Data = jQuery.parseJSON(data);                
                $('#save').prop('disabled',true);
            }
        });
        return false;
    })
    $('#search').on('keyup',function(){
        var selected    = $(this);
        var min         = parseInt($('#populationMin').text());
        var max         = parseInt($('#populationMax').text());
        var minTransp   = parseInt($('#transparencyMin').text());
        var maxTransp   = parseInt($('#transparencyMax').text());
        var isCity      = $('#city').is(':checked') ? 1 : 0;
        var county      = $('#param-A1').val();
        var query       = 'action=searchQuery'+'&query='+selected.val()+'&min='+min+'&max='+max+'&transparencyMin='+minTransp+'&transparencyMax='+maxTransp+'&A2='+isCity+'&A1='+county;
        
        return searchAjax(query,pointDefault);
    })

    $('#param-A1,#city',$('#searchArea')).on('change',function(){
        $('#search').keyup()
    })
    
    var opened = false;
    $('.navbar-toggler').click(function(){
        if (opened == false) {
            $('section').css('top','260px')
            opened = true;
        } else {
            $('section').css('top','56px')
            opened = false;
        }
    })
    
    if (!isMobile)
        $('span.point[rel="1"]').click()
    

    $('#search').autocomplete({
        //source : $('#searchSource').text().split('||'),
        source: function( request, response ) {
            var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
            response( $.grep( $('#searchSource').text().split('||'), function( value ) {
              value = value.label || value.value || value;
              return matcher.test( value ) || matcher.test( normalize( value ) );
            }) );
        },
        select : function(i,item) {
            var item        = item;
            var min         = parseInt($('#populationMin').text());
            var max         = parseInt($('#populationMax').text());
            var query       = 'action=searchQueryOne'+'&query='+item.item.value+'&min='+min+'&max='+max;
            
            $('#search').val(item.item.value);

            searchAjax(query).done(function(){
                $('.point[title="'+item.item.value+'"]').click();
            });
        }
    });
    
    $('#search').keyup();


})

function selectPoint(item) {
    selected = item;
    $('.point.selected').removeClass('selected');
    selected.addClass('selected')
    
    return selected;
}
function refreshPointPositions(actualZoom) {
    $('#itemList .point').each(function(i,item){
        j = $(item).attr('rel');
        
        $(item).width(pointWidth * actualZoom)
        $(item).height(pointHeight * actualZoom)
        
        $(item).css('top',pointTop[j] * actualZoom);
        $(item).css('left',pointLeft[j] * actualZoom);
    })
}
function searchAjax(query,pointDefault) {
    setTimeout(function(){ //need to delay the result, so the previous search doesnt come later than the previous
        return $.ajax({
            type    : 'post',
            url     : 'ajax.php',
            data    : query,
            async   : true,
            success : function(data) {
                //Data = jQuery.parseJSON(data);                
                
                $('#itemList').html(data)
                $('#itemList').append(pointDefault)
                
                $('#itemList .point').each(function(i,item){
                    j            = $(item).attr('rel');
                    pointTop[j]    = parseInt($(item).css('top'))
                    pointLeft[j]   = parseInt($(item).css('left'))
                })
                refreshPointPositions(actualZoom);
            }
        });
    }, 100);
    
}
function stopWheel(e){
    if(!e){ // IE7, IE8, Chrome, Safari
        e = window.event; 
    }
    if(e.preventDefault) { // Chrome, Safari, Firefox
        e.preventDefault(); 
    } 
    e.returnValue = false; // IE7, IE8
}                
