<?php 

    include_once 'connect.php';
    
    function transparencyValue($transparencyValue) {
        switch(true) {
            case $transparencyValue == 0:
                $transpVal = 'attr_0';
            break;
            case $transparencyValue < 25:
                $transpVal = 'attr_4';
            break;
            case $transparencyValue > 25 && $transparencyValue < 50:
                $transpVal = 'attr_3';
            break;
            case $transparencyValue > 50 && $transparencyValue < 75:
                $transpVal = 'attr_5';
            break;
            default:
                $transpVal = 'attr_1';
            break;
        }
        
        return $transpVal;
    }
  
    switch($_POST['action']) {
        case 'item':
            die(json_encode(Db::getInstance()->fetchOne('SELECT * FROM `item` WHERE `id` = :id',['id' => $_POST['id']])));    
        break;
        case 'itemParams':
            $data = Db::getInstance()->fetch('SELECT `name`,`value` FROM `item_parameter` JOIN `parameter` ON `parameter`.`id` = `item_parameter`.`parameter` WHERE `item_parameter`.`item` = :id',['id' => $_POST['id']]);

            foreach ($data as $k => $Param) {
                switch($Param->name) {
                    case 'E4':
                        $Param->value = number_format($Param->value,1);
                    break;
                }
            }
            
            die(json_encode($data));    
        break;
        case 'searchQuery':
            $whereCond           = [];
            $whereVars           = [];
            $paramIsCity         = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'A2']);
            $paramCounty         = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'A1']);
            $paramTransparency   = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'E4']);

            if ($_POST['A1']) {
               $whereCond[]                 = ' AND `item` IN (SELECT `item` FROM `item_parameter` WHERE `parameter` = :countyParamId AND `value` = :countyParamVal)';
               $whereVars['countyParamId']  = $paramCounty;
               $whereVars['countyParamVal'] = $_POST['A1'];
            }

            if ($_POST['A2']) {
               $whereCond[]                 = ' AND `item` IN (SELECT `item` FROM `item_parameter` WHERE `parameter` = :cityParamId AND `value` = :cityParamVal)';
               $whereVars['cityParamId']    = $paramIsCity;
               $whereVars['cityParamVal']   = 1;
            }

            if ($_POST['min'] != 0) {
               $whereCond[] = ' AND `i`.`population` BETWEEN :min AND :max';
               $whereVars['min']  = $_POST['min'];
               $whereVars['max']  = $_POST['max'];
            }
               
            $whereCond[]                         = ' AND `item` IN (SELECT `item` FROM `item_parameter` WHERE `parameter` = :transparencyParamId AND `value` BETWEEN '.$_POST['transparencyMin'].' AND '.$_POST['transparencyMax'].' OR `population` IS NULL)';
            $whereVars['transparencyParamId']    = $paramTransparency;

            $data = Db::getInstance()->fetch('SELECT `i`.* FROM `item` `i` LEFT JOIN `item_parameter` `ip` ON `i`.`id` = `ip`.`item` WHERE `i`.`name` LIKE :query collate utf8_general_ci' . implode(null,$whereCond) . ' GROUP BY `item`',['query' => '%'.$_POST['query'].'%'] + $whereVars);
                                           
            $transparencyId      = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'E4']);
            $webId               = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'web_link']);
            $isCityId            = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'A2']);
            $isCountyCityId      = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'A8']);
            $isDistrictCityId    = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'A9']);

            foreach ($data as $Item) {
                $webClass           = null;
                
                $transparencyValue  = Db::getInstance()->fetchSingle('SELECT `value` FROM `item_parameter` WHERE `item` = :id AND `parameter` = :paramId',['id' => $Item->id, 'paramId' => $transparencyId]);
                $webValue           = Db::getInstance()->fetchSingle('SELECT `value` FROM `item_parameter` WHERE `item` = :id AND `parameter` = :paramId',['id' => $Item->id, 'paramId' => $webId]);
                $isCityValue        = Db::getInstance()->fetchSingle('SELECT `value` FROM `item_parameter` WHERE `item` = :id AND `parameter` = :paramId',['id' => $Item->id, 'paramId' => $isCityId]);
                
                //echo "\r\n $Item->name = $isCityValue";
                $isCity             = $isCityValue ? 'square' : 'dot';
                //echo "\r\n $isCity";

                if ($webValue == '')
                    $webClass = 'noWeb';

                echo '<span class="point '.$isCity.' '.$webClass.' '.transparencyValue($transparencyValue).'" rel="'.$Item->id.'" style="top: '.$Item->x.'px; left: '.$Item->y.'px; " title="'.$Item->name.'"></span>';
            }
        break;
        case 'searchQueryOne':
            $Item            = Db::getInstance()->fetchOne('SELECT * FROM `item` WHERE `name` LIKE :query collate utf8_general_ci AND `population` BETWEEN :min AND :max OR `population` IS NULL',['query' => $_POST['query'], 'min' => $_POST['min'], 'max' => $_POST['max']]);
            $isCityId        = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'A2']);
            $transparencyId  = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'E4']);
            $webId               = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'web_link']);

            $isCityValue     = Db::getInstance()->fetchSingle('SELECT `value` FROM `item_parameter` WHERE `item` = :id AND `parameter` = :paramId',['id' => $Item->id, 'paramId' => $isCityId]);
            $isCity          = $isCityValue ? 'square' : 'dot';
           
            $webValue           = Db::getInstance()->fetchSingle('SELECT `value` FROM `item_parameter` WHERE `item` = :id AND `parameter` = :paramId',['id' => $Item->id, 'paramId' => $webId]);
            $isCityValue        = Db::getInstance()->fetchSingle('SELECT `value` FROM `item_parameter` WHERE `item` = :id AND `parameter` = :paramId',['id' => $Item->id, 'paramId' => $isCityId]);
            $transparencyValue  = Db::getInstance()->fetchSingle('SELECT `value` FROM `item_parameter` WHERE `item` = :id AND `parameter` = :paramId',['id' => $Item->id, 'paramId' => $transparencyId]);

            //echo "\r\n $Item->name = $isCityValue";
            $isCity             = $isCityValue ? 'square' : 'dot';
            //echo "\r\n $isCity";

           echo '<span class="point '.$isCity.' '.$webClass.' '.transparencyValue($transparencyValue).'" rel="'.$Item->id.'" style="top: '.$Item->x.'px; left: '.$Item->y.'px" title="'.$Item->name.'"></span>';
        break;
        case 'data':
            $Data = Db::getInstance()->fetchOne('SELECT * FROM `item` WHERE `id` = :id LIMIT 1',['id' => $_POST['id']]);
            
            function stringShorten($string, $maxLen) {
                return strlen($string) > 50 ? substr($string,0,50)."..." : $string;   
            }
            
            echo '
                <div style="font-size:12px">
                    <h5>'.$Data->name.'</h5>
                    <div>
                        <label>Počet obyvateľov: </label>
                        <span class="clearClick" id="load-population">'.$Data->population.'</span>
                    </div>
                    <div>
                        <label>Kvalita webového sídla: </label>
                        <span class="clearClick" id="load-quality">'.$Data->quality.'</span>
                    </div>
                    <div>
                        <label>Základné údaje: </label>
                        <span class="clearClick" id="load-A3">'.$Data->law.'</span>
                    </div>
                    <div>
                        <label>Doplňujúce údaje: </label>
                        <span class="clearClick" id="load-advanced_info"></span>
                    </div>
                    <div>
                        <label>Index transparentnosti samosprávy: </label>
                        <span class="clearClick" id="load-transparency">'.$Data->transparency.'</span>
                    </div>
                    <div>
                        <label>Web: </label>
                        <span><a class="clearClick" id="load-web" href="#'.stringShorten($Data->web, 15).'"></a></span>
                    </div>
                    <div>
                        <a href="#"><label>Zistenia</a></label>
                        <span><a href="#">PDF</a></span>
                    </div>
                </div>
            ';
            die;
            
            echo '<pre>';
            print_r(Db::getInstance()->fetchOne('SELECT * FROM `item` WHERE `id` = :id',['id' => $_POST['id']]));
            die;
          
        break;
        case 'add':
            foreach ($_POST['data'] as $key => $value)
                if ($value == '')
                    unset($_POST['data'][$key]);

            Db::getInstance()->insert('item',$_POST['data']);    
        break;
        case 'save':
            foreach ($_POST['data'] as $key => $value)
                if ($value == '')
                    unset($_POST['data'][$key]);
                    
            Db::getInstance()->update('item',$_POST['data'],'`id` = :id',['id' => $_POST['id']]);
            
            foreach ($_POST['parameter'] as $key => $value) {
                $paramId = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :key',['key' => $key]);

                Db::getInstance()->delete("item_parameter",'`parameter` = :paramId AND `item` = :id',['paramId' => $paramId, 'id' => $_POST['id']]);
                Db::getInstance()->insert("item_parameter",['item' => $_POST['id'], 'parameter' => $paramId, 'value' => $value]);
            }
            
            //echo '<pre>'; print_r($_POST); echo '</pre>';    
            //echo '<pre>'; print_r(Db::getInstance()); echo '</pre>';    
        break;
        /*
        case 'search':
           //die (json_encode(Db::getInstance()->fetch('SELECT * FROM `item` WHERE `population` BETWEEN :min AND :max',['min' => $_POST['min'], 'max' => $_POST['max']])));
           $data = Db::getInstance()->fetch('SELECT * FROM `item` WHERE `name` LIKE :query collate utf8_general_ci AND `population` BETWEEN :min AND :max',['query' => '%'.$_POST['query'].'%', 'min' => $_POST['min'], 'max' => $_POST['max']]);
           
           foreach ($data as $Item){
                echo '<span class="point attr_'.$Item->attribute.'" rel="'.$Item->id.'" style="top: '.$Item->x.'px; left: '.$Item->y.'px; z-index:10" title="'.$Item->name.'"></span>';
           }
        break;
        */
    }
