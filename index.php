<!doctype html>
    <html class="fixed" lang="sk-SK">
    <head>
        <!-- Basic -->
        <meta charset="UTF-8">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.css">
        <link rel="stylesheet" href="custom2.css?uid=<?php echo uniqid() ?>">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

        <?php
            $isMobile   = false;
            $useragent  = $_SERVER['HTTP_USER_AGENT'];

            if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
                $isMobile = true;
        ?>
        <title>Mapa Žilinský kraj</title>
    </head>
  
    <body style="overflow:auto">
        
        <nav class="navbar navbar-expand-md navbar-dark">
            <a class="navbar-brand" href="https://stavarina.sk/map4/">Otvorené mestá a obce - <br /> mapa transparentnosti ŽSK</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbyrToggler" aria-controls="navbyrToggler" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbyrToggler">
                <ul class="navbar-nav m-auto">
                    <li class="nav-item"><a class="nav-link" href="#">Tabuľky a porovnania</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Ako používať mapu</a></li>
                    <li class="nav-item"><a class="nav-link active" href="#">Mapa</a></li>
                    <li class="nav-item"><a class="nav-link" href="https://expositus.sk/o-nas/">O nás</a></li>
                    <li class="nav-item"><a class="nav-link" href="https://expositus.sk/o-projekte/">O projekte</a></li>
                    <li class="nav-item"><a class="nav-link" href="https://expositus.sk/kontakt/">Kontakty</a></li>
                </ul>
            </div>
            <a href="#"><img src="facebook.png" border="0" width="24" height="24" alt="facebook.png (329 bytes)" class="float-right" /></a>
        </nav>        

        <div class="row" style="float: left;" id="leftPannel">
            <span title="zbaliť" id="sidePannelToggler" class="  py-2 pb-3 btn btn-primary ">&laquo;</span>
            <div class="col-12 pr-0">
                    <div id="section" class="collapse <?php echo  $isMobile ? null : 'show' ?>" style="min-height: 600px; <?php echo !isset($adminForm) ? 'width:350px' : 'width:600px' ?> ">
                            <div id="sideMenu" class="">
                                <h5 class="p-2 px-3">Vyhľadávanie<button class=" btn btn-primary float-right btn-sm p-0 px-2 m-0 mr-0 ml-3 mb-1 " style="" type="button" data-toggle="collapse" data-target="#collapseMenu" aria-expanded="true" aria-controls="collapseMenu">-</button></h5>
                                <div id="collapseMenu" class="p-1 collapse <?php echo /* $isMobile ? null : */ 'show' ?>">
                                    <div id="searchArea">
                                        <hr class="separator d-none" />
                                       
                                        <div class="input-group ">
                                            <div class="input-group-prepend">
                                              <div class="input-group-text">Hľadať</div>
                                            </div>
                                            <input autofocus="autofocus" name="data[name]" type="text" class="form-control " id="search" placeholder="obec/mesto">
                                            <!-- <div class="input-group-append" > -->
                                              <div class="input-group-text" style="background-color: green; margin-left:-1px;border-radius:0"><button title="Vyhľadať"  type="button" class="btn btn-primary  btn-sm p-0  border-0">Ok</button></div>
                                            <!-- </div> -->
                                        </div>
                                    
                                        <div class="input-group mt-1">
                                            <div class="checkbox-custom checkbox-primary">
                                                <input style="float:left;margin-top:7px;margin-right:5px" name="parameter[city]" type="checkbox" id="city" value="1" />
                                                <label for="city" style="font-size:0.8em">Zobraziť iba mestá</label>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 my-2 mt-4 p-0 text-center population">
                                            <label for="amount">Počet obyvateľov</label>
                                            <span id="populationMin">0</span><span style=""> - </span><span id="populationMax">100000</span>
                                            <div id="slider-range" class="m-2"></div>                        
                                        </div>
                                        
                                        <div class="col-sm-12 my-2 mt-4 p-0 text-center transparency">
                                            <label for="amount">Index transparentnosti</label>
                                            <span id="transparencyMin">0</span><span style=""> - </span><span id="transparencyMax">100</span>
                                            <div id="slider-range-transparency" class="m-2"></div>                        
                                        </div>
                                        
                                        <div class="input-group mt-1">
                                            <div class="input-group-prepend">
                                              <div class="input-group-text">Okres</div>
                                            </div>
                                            <select name="A1" type="text" class="form-control" style="height: auto;" id="param-A1">
                                                <?php 
                                                    foreach ([0 => 'Všetky',1 => 'Bytča','Čadca','Dolný Kubín','Kysucké Nové Mesto','Liptovský Mikuláš','Martin','Námestovo','Ružomberok','Turčianske Teplice','Tvrdošín','Žilina'] as $id => $option) {
                                                        echo '<option value="'.$id.'">'.$option.'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="itemData" class="mt-4" style="display: none;">
                                <!-- Ajax data holder -->
                                
                                <div style="font-size:12px">
                                    <h5 id="load-name" class="p-2 px-3"></h5>
                                    <div>
                                        <label>Počet obyvateľov: </label>
                                        <span class="clearClick" id="load-population"></span>
                                    </div>
                                    <div>
                                        <label>Kvalita webového sídla: </label>
                                        <span class="clearClick" id="load-web_quality"></span>
                                    </div>
                                    <div>
                                        <label>Základné údaje: </label>
                                        <span class="clearClick" id="load-A3"></span>
                                    </div>
                                    <div>
                                        <label>Doplňujúce údaje: </label>
                                        <span class="clearClick" id="load-advanced_info"></span>
                                    </div>
                                    <div>
                                        <label>Index transparentnosti samosprávy: </label>
                                        <span class="clearClick" id="load-E4"></span>
                                    </div>
                                    <div>
                                        <label>Web: </label>
                                        <span><a class="clearClick" id="load-web_link" href="#"></a></span>
                                    </div>
                                    <div>
                                        <a href="#"><label>Zistenia</a></label>
                                        <span><a href="#">PDF</a></span>
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                        <?php  isset($adminForm) ? include_once('adminForm.php') : null; ?>
                        <hr class="separator my-4 mx-2" />
                            
                            <div id="legend" class="">
                                <h5 class="p-2 px-3">Legenda<button class=" btn btn-primary float-right btn-sm p-0 px-2 m-0 mr-0 ml-3 mb-1" type="button" data-toggle="collapse" data-target="#collapseLegend" aria-expanded="true" aria-controls="collapseLegend">-</button></h5>
                                <div id="collapseLegend" class="px-3 collapse <?php echo /* $isMobile ? null : */ 'show' ?>">
                                    
                                    <hr class="separator d-none" />
                                    <div>
                                        <span class="point square attr_3" style=""></span>
                                        <span>Krajské mesto</span>
                                    </div>
                                    <div>
                                        <span class="point square attr_2"></span>
                                        <span>Okresné mesto</span>
                                    </div>
                                    <div>
                                        <span class="point dot"></span>
                                        <span>Obec</span>
                                    </div>
                                </div>

                                <div id="score" class="mt-4 px-3">
                                    <h6>Hodnotenie</h6>
                                    <div>
                                        <span class="point square attr_1" style=""></span>/<span class="point dot attr_1" style=""></span>
                                        <span>výborné hodnotenie</span>
                                    </div>
                                    <div>
                                        <span class="point square attr_5" style=""></span>/<span class="point dot attr_5" style=""></span>
                                        <span>dobré hodnotenie</span>
                                    </div>
                                    <div>
                                        <span class="point square attr_3" style=""></span>/<span class="point dot attr_3" style=""></span>
                                        <span>dostatočné hodnotenie</span>
                                    </div>
                                    <div>
                                        <span class="point square attr_4" style=""></span>/<span class="point dot attr_4" style=""></span>
                                        <span>slabé hodnotenie</span>
                                    </div>
                                </div>

                                <hr class="separator" />
                                <div class="text-center">
                                    <a data-fancybox data-type="ajax" data-src="/zoznam-skratiek.html" href="javascript:;">
                                        zoznam  skratiek
                                    </a>
                                </div>                                
                            </div>

                        <hr class="separator my-4 mx-2" />

                            <div id="sponsors" class="">
                                <h5 class="p-2 px-3">Partneri</h5>
                                <div id="collapseLegend" class="px-3 collapse <?php echo /* $isMobile ? null : */ 'show' ?>">
                                        <img src="evs.png" style="max-width:220px;"  alt="Logo európskej únie" title="Logo európskej únie" class="m-auto img-responsive py-3" />
                                        <hr class="separator m-0" />
                                        <img src="esf.png" style="max-width:220px;" alt="Logo ESF" title="Logo ESF" class="m-auto img-responsive p-3" />
                                </div>    
                            </div>    
                    </div>
                
            </div>
        </div> 
        <div class="row">
                <div class="col-12" id="dataContainer">
                    <div id="itemList">
                        <?php
                            include_once 'connect.php';

                            /*
                            $transparencyId = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'E4']);
                            $webId          = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'web_link']);
                            $isCityId       = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'A2']);
                            $isCountyCityId       = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'A8']);
                            $isDistrictCityId       = Db::getInstance()->fetchSingle('SELECT `id` FROM `parameter` WHERE `name` = :name',['name' => 'A9']);
                            

                            foreach(Db::getInstance()->fetch('SELECT * FROM `item`') as $Item) {
                                $webClass           = null;
                                
                                $transparencyValue  = Db::getInstance()->fetchSingle('SELECT `value` FROM `item_parameter` WHERE `item` = :id AND `parameter` = :paramId',['id' => $Item->id, 'paramId' => $transparencyId]);
                                $webValue           = Db::getInstance()->fetchSingle('SELECT `value` FROM `item_parameter` WHERE `item` = :id AND `parameter` = :paramId',['id' => $Item->id, 'paramId' => $webId]);
                                $isCityValue        = Db::getInstance()->fetchSingle('SELECT `value` FROM `item_parameter` WHERE `item` = :id AND `parameter` = :paramId',['id' => $Item->id, 'paramId' => $isCityId]);
                                
                                //echo "\r\n $Item->name = $isCityValue";

                                $isCity             = $isCityValue ? 2 : 1;
                                
                                switch(true) {
                                    case $transparencyValue == 0:
                                        $transpVal = 0;
                                    break;
                                    case $transparencyValue < 25:
                                        $transpVal = 1;
                                    break;
                                    case $transparencyValue > 25 && $transparencyValue < 50:
                                        $transpVal = 2;
                                    break;
                                    case $transparencyValue > 50 && $transparencyValue < 75:
                                        $transpVal = 3;
                                    break;
                                    default:
                                        $transpVal = 4;
                                    break;
                                            
                                }
                                
                                //echo "\r\n<br> $Item->name = $transpVal ($transparencyValue)";
                                
                                
                                if ($webValue == '')
                                    $webClass = 'noWeb';
                                
                                echo '<span class="point attr_'.$isCity.' '.$webClass.' E4_'.$transpVal.'" rel="'.$Item->id.'" style="top: '.$Item->x.'px; left: '.$Item->y.'px; " title="'.$Item->name.'"></span>';
                            }
                            */

                        ?>
                        <div class="point" rel="0" id="pointDefault"></div>
                    </div>
                    <div id="zoomButtons">
                        <button title="Zväčšiť" id="zoomIn" type="button">+</button>
                        <button title="Zmenšiť" id="zoomOut" type="button">-</button>
                        <button title="Reset" id="reset" type="button">o</button>
                    </div>

                </div>
                <div style="clear: both;"></div>
        </div>

        <div id="footer" class="py-3 d-none" style="clear: both;">
            <img src="evs.png" style="max-height:70px"  alt="Logo európskej únie" title="Logo európskej únie" class="px-5 m-auto img-responsive" />
            <img src="esf.png" style="max-height:70px" alt="Logo ESF" title="Logo ESF" class="px-5 m-auto img-responsive" />
        </div>
        


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/qtip2/3.0.3/jquery.qtip.min.js"></script>

        <!-- <script src="touch.js"></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
        <script src="inits.js?<?php echo uniqid() ?>"></script>
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
        
    <?php             
        foreach (Db::getInstance()->fetchArray('SELECT `name` FROM `item`') as $city)
            $cities[] = $city['name'];
    ?>
        <div id="searchSource" class="d-none"><?php echo implode('||',$cities) ?></div>
    </body>
</html>